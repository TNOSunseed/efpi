# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

efpi xmpp software

### How do I get set up? ###
NOTE: the installation should be done on a clean image (no prior installations of java as this will stop the execution of the installation script)


1) wget --content-disposition 'https://drive.google.com/uc?export=download&id=0B-cFXq7fob7dSHdBT0hENTVZSGc'

2) tar xvf security-dependencies.tar.gzsecurity-dependencies.tar.gz

3) cd dependencies

4) sudo dpkg -i *

5) cd /home/edison

6) mkdir efpi (Create the directory /home/edison/efpi)

7) Copy the contents of the zip file to the efpi directory

8) cd /home/edison/efpi (ensure you are in the efpi directory)

9) sudo sh efpi.sh (run the efpi.sh from within the efpi directory as root)

10) During execution the script you will have to type Y at one point to continue installing java packages

11) ps aux|grep java (After execution efpi will be running as a service, verify there is a running java process)